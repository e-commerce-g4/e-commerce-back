package co.simplon.promo18.ecommerceback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ECommerceBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(ECommerceBackApplication.class, args);
	}

}
