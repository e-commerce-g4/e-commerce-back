package co.simplon.promo18.ecommerceback;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.github.javafaker.Faker;
import co.simplon.promo18.ecommerceback.entities.Address;
import co.simplon.promo18.ecommerceback.entities.Design;
import co.simplon.promo18.ecommerceback.entities.Item;
import co.simplon.promo18.ecommerceback.entities.Order;
import co.simplon.promo18.ecommerceback.entities.Product;
import co.simplon.promo18.ecommerceback.entities.Size;
import co.simplon.promo18.ecommerceback.entities.User;

@Component
public class FixtureLoader {
  @PersistenceContext
  private EntityManager em;
  @Autowired
  private PasswordEncoder encoder;

  @EventListener(ApplicationReadyEvent.class)
  @Transactional
  public void load() {
    Faker faker = new Faker(new Locale("fr"));
    List<Product> productList = new ArrayList<>();
    List<Size> sizeList = new ArrayList<>();
    List<Design> designList = new ArrayList<>();

    productList = createProducts(faker);

    sizeList = createSize();

    designList = createDesign();

    createUsers(productList, sizeList, designList, faker);
  }

  private List<Product> createProducts(Faker faker) {
    List<Product> productList = new ArrayList<>();

    String[] listGenders = {"Homme", "Femme", "Enfant"};
    String[] listColors = {"Noir", "Blanc", "Bleu", "Gris", "Rouge", "Vert", "Orange"};
    int iGender = 0;
    int iColor = 0;

    for (int i = 0; i < 20; i++) {

      String gender = listGenders[iGender];
      String color = listColors[iColor];

      String label = "T-Shirt " + gender.toLowerCase() + " de couleur " + color.toLowerCase();
      String ref = "TS-" + gender.toUpperCase() + "-" + color.toUpperCase();
      String img = "/assets/img/ts-" + color.toLowerCase() + ".png";

      Product product = new Product(label, ref, (float) faker.number().randomDouble(2, 15, 30),
          color, gender, img);

      em.persist(product);
      productList.add(product);

      iGender++;
      if (iGender >= listGenders.length) {
        iGender = 0;
        iColor++;
        if (iColor >= listColors.length) {
          iColor = 0;
        }
      }
    }
    return productList;
  }

  private List<Size> createSize() {
    List<Size> sizeList = new ArrayList<>();
    String[] listLabel = {"XS", "S", "M", "L", "XL", "XXL"};

    for (String label : listLabel) {
      Size size = new Size(label);
      em.persist(size);
      sizeList.add(size);
    }

    return sizeList;
  }

  private List<Design> createDesign() {
    List<Design> sizeList = new ArrayList<>();
    String[] listLabel =
        {"Ours", "Epée", "Ballon", "Château", "Nuage", "Extraterrestre", "Guitare"};

    for (String label : listLabel) {
      Design design = new Design(label, label + ".png");
      em.persist(design);
      sizeList.add(design);
    }

    return sizeList;
  }

  private void createUsers(List<Product> productList, List<Size> sizeList, List<Design> designList,
      Faker faker) {

    createUserTest("ROLE_USER", productList, sizeList, designList, faker);
    createUserTest("ROLE_ADMIN", productList, sizeList, designList, faker);

    for (int i = 0; i < 5; i++) {

      String firstName = faker.name().firstName();
      String lastName = faker.name().lastName();
      String email = faker.internet().emailAddress(username(firstName, lastName));
      String phone =
          StringUtils.deleteWhitespace(faker.phoneNumber().cellPhone().replace("+33", "0"));
      String password = encoder.encode("1234");

      User user = new User(firstName, lastName, phone, email, password, "ROLE_USER");

      em.persist(user);

      createAddress(user, faker);

      createOrder(user, productList, sizeList, designList, faker);
    }
  }

  private void createUserTest(String role, List<Product> productList, List<Size> sizeList,
      List<Design> designList, Faker faker) {
    String firstName = role.toLowerCase().substring(5) + "_firstname";
    String lastName = role.toLowerCase().substring(5) + "_lastname";;
    String email = role.toLowerCase().substring(5) + "@test.com";
    String phone =
        StringUtils.deleteWhitespace(faker.phoneNumber().cellPhone().replace("+33", "0"));
    String password = encoder.encode("1234");

    User user = new User(firstName, lastName, phone, email, password, role);

    em.persist(user);

    createAddress(user, faker);

    createOrder(user, productList, sizeList, designList, faker);
  }


  private String username(String firstName, String lastName) {
    String username = StringUtils.join(firstName.replaceAll("'", "").toLowerCase(), ".",
        lastName.replaceAll("'", "").toLowerCase());

    return StringUtils.deleteWhitespace(username);
  }

  private void createAddress(User user, Faker faker) {
    String[] listLabel = {"Domicile", "Travail",};

    for (String label : listLabel) {

      Address address = new Address(label, faker.address().streetAddress(),
          faker.address().zipCode(), faker.address().cityName(), "France", user);

      em.persist(address);
      user.getAddresses().add(address);
    }
  }

  private void createOrder(User user, List<Product> productList, List<Size> sizeList,
      List<Design> designList, Faker faker) {

    for (int i = 0; i < new Random().nextInt(7) + 3; i++) {
      Order order = new Order(user,
          user.getAddresses().toArray(new Address[user.getAddresses().size()])[new Random()
              .nextInt(2)],
          "CB", "Expédiée", LocalDateTime.ofInstant(
              faker.date().past(300, TimeUnit.DAYS).toInstant(), ZoneId.systemDefault()));


      em.persist(order);
      user.getOrders().add(order);

      createItem(order, productList, sizeList, designList, faker);
    }
  }

  private void createItem(Order order, List<Product> productList, List<Size> sizeList,
      List<Design> designList, Faker faker) {

    for (int i = 0; i <= new Random().nextInt(5); i++) {
      Product product = productList.get(new Random().nextInt(productList.size()));
      Size size = sizeList.get(new Random().nextInt(sizeList.size()));
      Design design = designList.get(new Random().nextInt(designList.size()));

      Item item = new Item(order, product, new Random().nextInt(3) + 1, size, design,
          (float) faker.number().randomDouble(2, 15, 30));

      em.persist(item);
      order.getItems().add(item);
    }
  }
}
