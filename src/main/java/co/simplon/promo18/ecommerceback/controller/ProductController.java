package co.simplon.promo18.ecommerceback.controller;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.ecommerceback.entities.Product;
import co.simplon.promo18.ecommerceback.repository.ProductRepository;

@RestController
@RequestMapping("/api/product")
public class ProductController {

  @Autowired
  private ProductRepository productRepo;

  @GetMapping
  public Page<Product> getAll(@RequestParam(required = false) String color,
      @RequestParam(required = false) String gender,
      @RequestParam(required = false, defaultValue = "0") int page,
      @RequestParam(required = false, defaultValue = "10") int pageSize) {
    if (color != null && gender != null) {
      return productRepo.findByColorLikeAndGenderLike(color, gender,
          PageRequest.of(page, pageSize));
    }
    if (color != null && gender == null) {
      return productRepo.findByColorLike(color, PageRequest.of(page, pageSize));
    }
    if (color == null && gender != null) {
      return productRepo.findByGenderLike(gender, PageRequest.of(page, pageSize));
    }
    return productRepo.findAll(PageRequest.of(page, pageSize));
  }

  @GetMapping("/{id}")
  public Product getOne(@PathVariable int id) {

    return productRepo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  @GetMapping("/color")
  public Set<String> getColors() {
    return productRepo.findDistinctColor();
  }

  @GetMapping("/gender")
  public Set<String> getGenders() {
    return productRepo.findDistinctGender();
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    Product toDelete = getOne(id);
    productRepo.delete(toDelete);
  }

  @PutMapping("/{id}")
  public Product update(@PathVariable int id, @ModelAttribute @Valid Product product,
      @ModelAttribute MultipartFile upload) {
    System.out.println(upload);
    Product toUpdate = getOne(id);
    toUpdate.setLabel(product.getLabel());
    toUpdate.setReference(product.getReference());
    toUpdate.setPrice(product.getPrice());
    toUpdate.setColor(product.getColor());
    toUpdate.setGender(product.getGender());
    if (upload != null) {
      toUpdate.setImage(savePicture(upload));
    }
    return productRepo.save(toUpdate);
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Product add(@ModelAttribute @Valid Product product, @ModelAttribute MultipartFile upload) {
    product.setId(null);

    // On stock le nom du fichier sur la base de donnée pour pouvoir savoir quel fichier est lié à
    // quel Post
    product.setImage(savePicture(upload));
    productRepo.save(product);
    return product;
  }

  private String savePicture(MultipartFile upload) {
    // On créer un nom unique pour le fichier pour éviter les collisions de noms (genre 2 personnes
    // qui upload un fichier chat.jpg)
    // et on fait en sorte de récupérer l'extension du fichier original pour la concaténer à ce nom
    String filename =
        UUID.randomUUID() + "." + FilenameUtils.getExtension(upload.getOriginalFilename());

    try {
      // On transfère le fichier dans le fichier de la requête dans le dossier d'upload, avec son
      // nouveau nom
      upload.transferTo(new File(getUploadFolder(), filename));
    } catch (IllegalStateException | IOException e) {
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "File upload failed");
    }

    return filename;
  }

  private File getUploadFolder() {
    File folder =
        new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
    if (!folder.exists()) {
      folder.mkdirs();
    }
    return folder;
  }
}
