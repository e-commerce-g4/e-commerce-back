package co.simplon.promo18.ecommerceback.controller;

import java.util.Set;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.entities.Address;
import co.simplon.promo18.ecommerceback.entities.User;
import co.simplon.promo18.ecommerceback.repository.AddressRepository;

@RestController
@RequestMapping("/api/address")
public class AddressController {
  @Autowired
  private AddressRepository addressRepo;

  @GetMapping
  public Set<Address> getAll(@AuthenticationPrincipal User user) {
    return addressRepo.findByUser(user);
  }

  @GetMapping("/{id}")
  public Address getOne(@PathVariable int id, @AuthenticationPrincipal User user) {
    return addressRepo.findByIdAndUser(id, user)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  @PostMapping
  public Address add(@Valid @RequestBody Address address, @AuthenticationPrincipal User user) {
    System.out.println(address);
    address.setId(null);
    address.setUser(user);
    addressRepo.save(address);
    return address;
  }

  @PostMapping("/{id}")
  public Address update(@PathVariable int id, @Valid @RequestBody Address address,
      @AuthenticationPrincipal User user) {
    System.out.println(address);
    Address toUpdate = getOne(id, user);
    checkOwner(toUpdate.getUser().getId(), user.getId());
    toUpdate.setLabel(address.getLabel());
    toUpdate.setStreet(address.getStreet());
    toUpdate.setZipCode(address.getZipCode());
    toUpdate.setCity(address.getCity());
    toUpdate.setCountry(address.getCountry());
    addressRepo.save(toUpdate);
    return toUpdate;
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
    Address toDelete = getOne(id, user);
    checkOwner(toDelete.getUser().getId(), user.getId());
    // List<Order> orders = orderRepo.findByAddress(toDelete);
    // for (Order order : orders) {
    // order.setAddress(null);
    // orderRepo.save(order);
    // }
    // addressRepo.delete(toDelete);
    toDelete.setUser(null);
    addressRepo.save(toDelete);
  }

  public void checkOwner(int requestId, int userId) {
    if (requestId != userId) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
  }
}
