package co.simplon.promo18.ecommerceback.controller;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.entities.Order;
import co.simplon.promo18.ecommerceback.entities.User;
import co.simplon.promo18.ecommerceback.repository.OrderRepository;

@RestController
@RequestMapping("/api/order")
public class OrderController {

  @Autowired
  private OrderRepository orderRepo;

  @GetMapping
  public Page<Order> getAll(@AuthenticationPrincipal User user,
      @RequestParam(required = false, defaultValue = "0") int page,
      @RequestParam(required = false, defaultValue = "10") int pageSize) {
    return orderRepo.findByUser(user, PageRequest.of(page, pageSize));
  }

  @GetMapping("/{id}")
  public Order getOne(@AuthenticationPrincipal User user, @PathVariable int id) {
    Order order =
        orderRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    if (order.getUser().getId() != user.getId()) {
      throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
    }
    return order;
  }

  @PostMapping("/new")
  public Order add(@RequestBody Order order) {
    order.setDate(LocalDateTime.now());
    order.setStatus("En attente");
    orderRepo.save(order);
    order.getItems().forEach(item -> {
      item.setOrder(order);
      item.setPrice(item.getProduct().getPrice());;
    });
    orderRepo.save(order);
    return order;
  }
}
