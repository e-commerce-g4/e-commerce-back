package co.simplon.promo18.ecommerceback.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo18.ecommerceback.entities.Design;
import co.simplon.promo18.ecommerceback.repository.DesignRepository;

@RestController
@RequestMapping("/api/design")
public class DesignController {
  @Autowired
  private DesignRepository designRepo;

  @GetMapping
  public List<Design> getAll() {
    return designRepo.findAll();
  }

}
