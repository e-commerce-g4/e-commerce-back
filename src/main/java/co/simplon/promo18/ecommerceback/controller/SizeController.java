package co.simplon.promo18.ecommerceback.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.ecommerceback.entities.Size;
import co.simplon.promo18.ecommerceback.repository.SizeRepository;

@RestController
@RequestMapping("/api/size")
public class SizeController {
  
  @Autowired
  private SizeRepository sizeRepo;

  @GetMapping
  public List<Size> getAll() {
    return sizeRepo.findAll();
  }
}
