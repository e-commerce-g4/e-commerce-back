package co.simplon.promo18.ecommerceback.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Item {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @ManyToOne
  @JsonIgnore
  private Order order;
  @NotNull
  @ManyToOne
  private Product product;
  @Min(1)
  @NotNull
  private Integer quantity;
  @NotNull
  @ManyToOne
  private Size size;
  @NotNull
  @ManyToOne
  private Design design;
  private Float price;

  public Item() {}

  public Item(Integer quantity, Float price) {
    this.quantity = quantity;
    this.price = price;
  }

  public Item(Order order, Product product, Integer quantity, Size size, Design design,
      Float price) {
    this.order = order;
    this.product = product;
    this.quantity = quantity;
    this.size = size;
    this.design = design;
    this.price = price;
  }

  public Item(Integer id, Order order, Product product, Integer quantity, Size size, Design design,
      Float price) {
    this.id = id;
    this.order = order;
    this.product = product;
    this.quantity = quantity;
    this.size = size;
    this.design = design;
    this.price = price;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Size getSize() {
    return size;
  }

  public void setSize(Size size) {
    this.size = size;
  }

  public Design getDesign() {
    return design;
  }

  public void setDesign(Design design) {
    this.design = design;
  }

  public Float getPrice() {
    return price;
  }

  public void setPrice(Float price) {
    this.price = price;
  }

  @Override
  public String toString() {
    return "Item [design=" + design + ", id=" + id + ", order=" + order + ", price=" + price
        + ", product=" + product + ", quantity=" + quantity + ", size=" + size + "]";
  }
}
