package co.simplon.promo18.ecommerceback.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Design {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @NotBlank
  private String label;
  private String image;
  @OneToMany(mappedBy = "design")
  @JsonIgnore
  private Set<Item> items = new HashSet<>();

  public Design() {}

  public Design(String label, String image) {
    this.label = label;
    this.image = image;
  }

  public Design(Integer id, String label, String image) {
    this.id = id;
    this.label = label;
    this.image = image;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Set<Item> getItems() {
    return items;
  }

  public void setItems(Set<Item> items) {
    this.items = items;
  }

  @Override
  public String toString() {
    return "Design [id=" + id + ", image=" + image + ", items=" + items + ", label=" + label + "]";
  }
}
