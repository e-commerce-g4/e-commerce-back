package co.simplon.promo18.ecommerceback.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Address {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @NotBlank
  private String label;
  @NotBlank
  private String street;
  @NotBlank
  private String zipCode;
  @NotBlank
  private String city;
  private String country;
  @ManyToOne
  @JsonIgnore
  private User user;
  @OneToMany(mappedBy = "address")
  @JsonIgnore
  private Set<Order> orders = new HashSet<>();

  public Address() {}

  public Address(String label, String street, String zipCode, String city, String country) {
    this.label = label;
    this.street = street;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
  }

  public Address(String label, String street, String zipCode, String city, String country,
      User user) {
    this.label = label;
    this.street = street;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
    this.user = user;
  }

  public Address(Integer id, String label, String street, String zipCode, String city,
      String country) {
    this.id = id;
    this.label = label;
    this.street = street;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return "Address [city=" + city + ", country=" + country + ", id=" + id + ", label=" + label
        + ", orders=" + orders + ", street=" + street + ", user=" + user + ", zipCode=" + zipCode
        + "]";
  }

}
