package co.simplon.promo18.ecommerceback.entities;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "order_table")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @NotNull
  @ManyToOne
  @JsonIgnoreProperties("orders")
  private User user;
  @ManyToOne
  private Address address;
  private String payment;
  private String status;
  private LocalDateTime date;
  @OneToMany(mappedBy = "order", cascade = CascadeType.PERSIST)
  @JsonIgnoreProperties("order")
  private Set<Item> items = new HashSet<>();

  public Order() {}

  public Order(User user, Address address, String payment, String status, LocalDateTime date) {
    this.user = user;
    this.address = address;
    this.payment = payment;
    this.status = status;
    this.date = date;
  }

  public Order(User user, Address address, String payment, String status, LocalDateTime date,
      Set<Item> items) {
    this.user = user;
    this.address = address;
    this.payment = payment;
    this.status = status;
    this.date = date;
    this.items = items;
  }

  public Order(Integer id, User user, Address address, String payment, String status,
      LocalDateTime date, Set<Item> items) {
    this.id = id;
    this.user = user;
    this.address = address;
    this.payment = payment;
    this.status = status;
    this.date = date;
    this.items = items;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public String getPayment() {
    return payment;
  }

  public void setPayment(String payment) {
    this.payment = payment;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public Set<Item> getItems() {
    return items;
  }

  public void setItems(Set<Item> items) {
    this.items = items;
  }

  @Override
  public String toString() {
    return "Order [address=" + address + ", date=" + date + ", id=" + id + ", items=" + items
        + ", payment=" + payment + ", status=" + status + ", user=" + user + "]";
  }

}
