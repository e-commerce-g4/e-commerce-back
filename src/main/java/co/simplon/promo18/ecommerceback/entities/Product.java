package co.simplon.promo18.ecommerceback.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @NotBlank
  private String label;
  private String reference;
  @NotNull
  private Float price;
  @NotBlank
  private String color;
  private String gender;
  private String image;
  @OneToMany(mappedBy = "product")
  @JsonIgnore
  private Set<Item> items = new HashSet<>();

  public Product() {}

  public Product(String label, String reference, Float price, String color, String gender,
      String image) {
    this.label = label;
    this.reference = reference;
    this.price = price;
    this.color = color;
    this.gender = gender;
    this.image = image;
  }

  public Product(Integer id, String label, String reference, Float price, String color,
      String gender, String image) {
    this.id = id;
    this.label = label;
    this.reference = reference;
    this.price = price;
    this.color = color;
    this.gender = gender;
    this.image = image;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public Float getPrice() {
    return price;
  }

  public void setPrice(Float price) {
    this.price = price;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String genre) {
    this.gender = genre;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Set<Item> getItems() {
    return items;
  }

  public void setItems(Set<Item> items) {
    this.items = items;
  }

  @Override
  public String toString() {
    return "Product [color=" + color + ", gender=" + gender + ", id=" + id + ", image=" + image
        + ", items=" + items + ", label=" + label + ", price=" + price + ", reference=" + reference
        + "]";
  }
}
