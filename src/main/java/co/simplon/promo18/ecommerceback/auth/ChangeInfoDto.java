package co.simplon.promo18.ecommerceback.auth;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ChangeInfoDto {
  @NotNull
  public Integer id;
  @NotBlank
  public String firstName;
  @NotBlank
  public String lastName;
  @NotBlank
  public String phone;
  @Email
  @NotBlank
  public String email;
}
