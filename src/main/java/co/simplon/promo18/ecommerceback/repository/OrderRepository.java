package co.simplon.promo18.ecommerceback.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.ecommerceback.entities.Address;
import co.simplon.promo18.ecommerceback.entities.Order;
import co.simplon.promo18.ecommerceback.entities.User;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
  Page<Order> findByUser(User user, Pageable pageable);
  List<Order> findByAddress(Address address);
}
