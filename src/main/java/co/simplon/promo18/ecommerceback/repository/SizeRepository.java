package co.simplon.promo18.ecommerceback.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.ecommerceback.entities.Size;

@Repository
public interface SizeRepository extends JpaRepository<Size, Integer> {
  
}
