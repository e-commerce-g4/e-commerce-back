package co.simplon.promo18.ecommerceback.repository;

import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.ecommerceback.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    Page<Product> findByColorLike(String color, PageRequest page);

    Page<Product> findByGenderLike(String gender, PageRequest page);

    Page<Product> findByColorLikeAndGenderLike(String color, String gender, PageRequest page);

    @Query("SELECT DISTINCT color FROM Product")
    Set<String> findDistinctColor();

    @Query("SELECT DISTINCT gender FROM Product")
    Set<String> findDistinctGender();
}
