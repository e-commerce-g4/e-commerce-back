package co.simplon.promo18.ecommerceback.repository;

import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.ecommerceback.entities.Address;
import co.simplon.promo18.ecommerceback.entities.User;

@Repository
public interface AddressRepository extends JpaRepository <Address, Integer> {
  Set<Address> findByUser(User user);
  Optional<Address> findByIdAndUser(int id,User user);
}
